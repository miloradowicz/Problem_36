
#include <algorithm>
#include <list>
#include <iostream>

using std::sort;
using std::list;
using std::cin;
using std::cout;
using std::endl;

struct Game {
  unsigned int _id;
  unsigned int _start;
  unsigned int _length;

  static bool Compare(const Game &g1, const Game &g2) {
    if (g1._start + g1._length == g2._start + g2._length)
      return (g1._start > g2._start);
    else
      return (g1._start + g1._length < g2._start + g2._length);
  }
};

int main(int argc, char **argv) {
  unsigned int num, start, length;
  list<Game> watchlist;
  Game *game_list;

  cin >> num;
  game_list = new Game[num];
  for (unsigned int u = 0; u < num; u++) {
    cin >> start >> length;
    game_list[u] = Game{ u, start, length };
  }

  sort(game_list, game_list + num, Game::Compare);

  unsigned int end = 0;
  for (unsigned int u = 0; u < num; u++) {
    if (game_list[u]._start >= end) {
      watchlist.push_back(game_list[u]);
      end = game_list[u]._start + game_list[u]._length;
    }
  }

  cout << watchlist.size() << endl;
  for (list<Game>::iterator it = watchlist.begin(); it != watchlist.end(); it++) {
    cout << (*it)._id << ' ';
  }

  return 0;
}